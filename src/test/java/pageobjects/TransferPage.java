package pageobjects;

import org.apache.commons.math3.analysis.function.Exp;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.xml.xpath.XPath;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class TransferPage {

    private WebDriver driver;
    private WebDriverWait wait;

    // Constructor
    public TransferPage(WebDriver browser) {
        driver = browser;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(60));
    }

    // Locators
    @FindBy(id = "user")
    private WebElement txtUser;
    @FindBy(id = "digital-key")
    private WebElement txtPassword;
    @FindBy(css = "button[class='button__type--primary button__type--primary--fill button__size--md button__width--full']")
    private WebElement btnIniciar;
    @FindBy(css = "div[class='text --small --alert']")
    private WebElement lblMensajeValidacion;
    @FindBy(css = "h3[class='title-user-blocked']")
    private WebElement lblUsuarioBloqueado;
    @FindBy(css = "h2[class='user-name']")
    private WebElement lblUsariologueado;
    @FindBy(xpath = "//ui-option-operation[@label='Transferir Dinero']//div//button//ui-icon[@class='hydrated']")
    private WebElement btnTransferencia;
    @FindBy(css = "button[class='button__type--primary button__type--primary--fill button__size--md button__width--full']")
    private WebElement btnNuevatransferencia;
    @FindBy(xpath = "//ui-item-operation[@title-item='A otras Cuentas Pichincha']")
    private WebElement btnTransferOtrasCuentas;
    @FindBy(xpath = "//ui-item-operation[@title-item='A otros bancos']")
    private WebElement btnTransferBancos;
    @FindBy(xpath = "//ui-item-operation[@title-item='Entre mis cuentas Pichincha']")
    private WebElement btnTransferPropia;
    @FindBy(xpath = "//ui-select-detail[@label='Transfieres desde:']//div[@class='select-detail__item-selected select-detail__item-selected--icon']")
    private WebElement btnTransfieresdesde;
    @FindBy(xpath = "//ui-select-detail[@label='Transfieres desde:']//div[@class='select-detail']//ul[@class='select-detail__items']")
    private WebElement cboSeleccionaDesde;
    @FindBy(css = "input[id='cardNumberAccount-0']")
    private WebElement txtCuentaDestino;
    @FindBy(css = "input[id='text']")
    private WebElement txtMonto;
    @FindBy(css = "input[id='party-account']")
    private WebElement txtOtraCuenta;
    @FindBy(css = "label[class='option option-1']")
    private WebElement opcInmediata;
    @FindBy(css = "label[class='option option-2']")
    private WebElement opcPorHorarios;
    @FindBy(css = "input[name='user'][id='user']")
    private WebElement txtDescripcionOperacion;
    @FindBy(id = "btn_transfers_1")
    private WebElement btnSiguiente;
    @FindBy(id = "cardNumber-0")
    private WebElement txtOtp;
    @FindBy(css = "span[class='slider round']")
    private WebElement optFavorito;
    @FindBy(id = "btn_transfers_2")
    private WebElement btnConfirmarTransfer;
    @FindBy(id = "favorite")
    private WebElement txtFavorito;
    @FindBy(css = "div[class='toggle-button b2 button-10']")
    private WebElement opcMoneda;
    @FindBy(id = "btn_save_favorite")
    private WebElement btnGuardarFavorito;
    @FindBy(xpath = "//div[@class='item-detail-transfer']//div[@class='item-detail-transfer-description']//span[@class='title'][text()='Transferencia exitosa']")
    private WebElement lblTransferexitoso;
    @FindBy(css = "p[class='description-operation']")
    private WebElement lbloperacion;
    @FindBy(id = "btn_go_home")
    private WebElement btnVolverInicio;
    @FindBy(xpath = "//div[@class='select-detail']//div[@class='select-detail__item-selected select-detail__item-selected--icon']")
    private WebElement cboCuentaOrigen;
    @FindBy(css = "ui-text[class='input-text-info__error text text--size-small text--color-error-500 text--weight-medium text--align-left hydrated']")
    private WebElement lblErrorCuenta;
    @FindBy(xpath = "//div[@class='content-item-operation --left']//div[@class='item-operation__info']//p[@class='title-item-operation'][text()='otrobanco3']")
    private WebElement opcOperacionFavorito;
    @FindBy(css = "button[class='button__type--primary button__type--primary--fill button__size--xs button__width--auto']")
    private WebElement btnOperacionFavorito;
    @FindBy(xpath = "//div[@class='container-button-link ng-tns-c72-2 ng-star-inserted']")
    private WebElement btnVerTodoFavorito;
    @FindBy(css = "input[class='searchbar-text searchbar-text--icon']")
    private WebElement txtbuscaFavorito;

    // Actions

    public void writeUsuario(String textoUsu) {
        wait.until(ExpectedConditions.elementToBeClickable(txtUser)).sendKeys(textoUsu);
    }
    public void writePassword(String letra){
        WebElement btnNumero = driver.findElement(By.xpath("//button[@class='button-number']//label[@class='label-button-number'][text()='"+ letra+"']"));
        wait.until(ExpectedConditions.elementToBeClickable(btnNumero)).click();
    }
    public void clickPassword() {
        wait.until(ExpectedConditions.elementToBeClickable(txtPassword)).click();
    }
    public void clickInicio() {
        wait.until(ExpectedConditions.elementToBeClickable(btnIniciar)).click();
    }
    public String getObtenerMensajeValidacion (){
        return wait.until(ExpectedConditions.elementToBeClickable(lblMensajeValidacion)).getText().trim();
    }
    public String getObtenerUsuarioLogueado (){
        return wait.until(ExpectedConditions.elementToBeClickable(lblUsariologueado)).getText().trim();
    }
    public void clickTransferencia() {
        wait.until(ExpectedConditions.elementToBeClickable(btnTransferencia)).click();
    }

    public void clickNuevaTransferencia() {
        wait.until(ExpectedConditions.elementToBeClickable(btnNuevatransferencia)).click();
    }
    public Integer getObtenerValorNuevaTransferencia(){
        Integer existeElemento = driver.findElements(By.cssSelector("button[class='button__type--primary button__type--primary--fill button__size--md button__width--full']")).size();
        return existeElemento;
    }
    public Integer getObtenerValorOTP(){
        Integer existeElemento = driver.findElements(By.id("cardNumber-0")).size();
        return existeElemento;
    }
    public void clickTransferPropia(){
        wait.until(ExpectedConditions.elementToBeClickable(btnTransferPropia)).click();
    }
    public void clickTransferBanco(){
        wait.until(ExpectedConditions.elementToBeClickable(btnTransferBancos)).click();
    }
    public void clickTransferOtrasCuentas(){
        wait.until(ExpectedConditions.elementToBeClickable(btnTransferOtrasCuentas)).click();
    }
    public void clickTransferDesde(){
        wait.until(ExpectedConditions.elementToBeClickable(btnTransfieresdesde)).click();
    }
    public void writeCuentaDestino(String cuenta) {
        wait.until(ExpectedConditions.elementToBeClickable(txtCuentaDestino)).sendKeys(cuenta);
    }
    public void writeMonto(String monto){
        wait.until(ExpectedConditions.elementToBeClickable(txtMonto)).sendKeys(monto);
    }
    public void writeOtraCuenta(String ocuenta){
        wait.until(ExpectedConditions.elementToBeClickable(txtOtraCuenta)).sendKeys(ocuenta);
    }
    public void clickSeleccionaInmediato(){
        wait.until(ExpectedConditions.elementToBeClickable(opcInmediata)).click();
    }
    public void clickSeleccionaPorHorarios(){
        wait.until(ExpectedConditions.elementToBeClickable(opcPorHorarios)).click();
    }
    public void writeDescripcionOperacion(String descripcion){
        wait.until(ExpectedConditions.elementToBeClickable(txtDescripcionOperacion)).sendKeys(descripcion);
    }
    public void clickSiguientePantalla(){
        wait.until(ExpectedConditions.elementToBeClickable(btnSiguiente)).click();
    }
    public void writeIngresaOTP(String otp){
        wait.until(ExpectedConditions.elementToBeClickable(txtOtp)).sendKeys(otp);
    }
    public void clickSeleccionaFavorito(){
        wait.until(ExpectedConditions.elementToBeClickable(optFavorito)).click();
    }
    public void clickSeleccionaMoneda(){
        wait.until(ExpectedConditions.elementToBeClickable(opcMoneda)).click();
    }
    public void clickPantallaConfirmar(){
        wait.until(ExpectedConditions.elementToBeClickable(btnConfirmarTransfer)).click();
    }
    public void writeFavorito(String favorito){
        wait.until(ExpectedConditions.elementToBeClickable(txtFavorito)).sendKeys(favorito);
    }
    public void clickGuardarFavorito(){
        wait.until(ExpectedConditions.elementToBeClickable(btnGuardarFavorito)).click();
    }
    public String getObtenerConfirmacionTransferencia(){
        return wait.until(ExpectedConditions.elementToBeClickable(lblTransferexitoso)).getText().trim();
    }
    public String getObtenerOperacion(){
        return wait.until(ExpectedConditions.elementToBeClickable(lbloperacion)).getText().trim();
    }
    public void clickVolverInicio(){
        wait.until(ExpectedConditions.elementToBeClickable(btnVolverInicio)).click();
    }

    public void clickSeleccionaOrigen(String posicion){
        WebElement cboselecciona = driver.findElement(By.xpath("//div[@class='select-detail']//div[@class='select-detail__item-selected select-detail__item-selected--icon']"));
        //WebElement cboselecciona = driver.findElement(By.xpath("//div[@class='select-detail']//div[@class='select-detail__item-selected select-detail__item-selected--icon']"));
        cboselecciona.click();
        By mySelector = By.xpath("//ui-select-detail[@label='Transfieres desde:']//div[@class='select-detail']//ul[@class='select-detail__items']//li[@class='select-detail__item'][" + posicion + "]");
        //By mySelector = By.xpath("//div[@class='select-detail']//ul[@class='select-detail__items']//li[@class='select-detail__item'][" + posicion + "]");
        List<WebElement> myElements = driver.findElements(mySelector);
        for(WebElement e : myElements) {
            e.click();
        }
    }

    public void clickSeleccionaDestino(String posicion2){
        WebElement cboselecciona2 = driver.findElement(By.xpath("//div[@class='select-detail']//div[@class='select-detail__item-selected select-detail__item-selected--icon']"));
        cboselecciona2.click();
        By mySelector2 = By.xpath("//ui-select-detail[@label='Destino:']//div[@class='select-detail']//ul[@class='select-detail__items']//li[@class='select-detail__item'][" + posicion2+ "]");
        List<WebElement> myElements2 = driver.findElements(mySelector2);
        for(WebElement e2 : myElements2) {
            e2.click();
        }
    }

    public String getObtenerErrorCuenta(){
        return wait.until(ExpectedConditions.elementToBeClickable(lblErrorCuenta)).getText().trim();
    }
    public void clickVerTodoFavorito(){
        wait.until(ExpectedConditions.elementToBeClickable(btnVerTodoFavorito)).click();
    }
    public void writeBuscaFavorito(String favorito) {
        wait.until(ExpectedConditions.elementToBeClickable(txtbuscaFavorito)).sendKeys(favorito);
    }
    public void clickOperacionFavorito(){
        wait.until(ExpectedConditions.elementToBeClickable(btnOperacionFavorito)).click();
    }

}
