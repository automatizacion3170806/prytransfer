@TransferenciaOtrasCuentas
 Feature: Proceso de transferencia a otras cuentas Pichincha.
   Aqui se realiza el proceso de transferencias a otras cuentas Pichincha en soles y dólares.

  Scenario: Transferencia a otras cuentas Pichincha de una cuenta de soles a soles
  Verificar que se pueda realizar una transferencia a terceros de una cuenta de soles a soles

    Given   El cliente abre la pagina de HBK
    When    Se ingresa el usuario "<usuario>" y password "<password>"
    Then    Clic en boton iniciar sesion y selecciona transferencia a otras cuentas
    When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" monto "<monto>" y descripcion "<descripcion>"
    Then    Se ingresa OTP "<otp>" y se confirma transferencia a otras cuentas
    When    Se confirma y valida resultado "<resultado>" y "<operacion>" a otras cuentas
    Examples: Parametros de entrada
      | usuario | password | origen | destino | moneda | monto | otp | resultado | operacion |
      | 1       | 1        | 1      | 1       | S      | 1     | 1   | 1         | 1         |

   Scenario: Transferencia a otras cuentas Pichincha de una cuenta de dólares a dólares
   Verificar que se pueda realizar una transferencia a terceros de una cuenta de dólares a dólares

     Given   El cliente abre la pagina de HBK
     When    Se ingresa el usuario "<usuario>" y password "<password>"
     Then    Clic en boton iniciar sesion y selecciona transferencia a otras cuentas
     When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" monto "<monto>" y descripcion "<descripcion>"
     Then    Se ingresa OTP "<otp>" y se confirma transferencia a otras cuentas
     When    Se confirma y valida resultado "<resultado>" y "<operacion>" a otras cuentas
     Examples: Parametros de entrada
       | usuario | password | origen | destino | moneda | monto | otp | resultado | operacion |
       | 2       | 2        | 2      | 2       | D      | 2     | 2   | 2         | 2         |

   Scenario: Transferencia a otras cuentas Pichincha de una cuenta de soles a dólares
   Verificar que se pueda realizar una transferencia a terceros de una cuenta de soles a dólares

     Given   El cliente abre la pagina de HBK
     When    Se ingresa el usuario "<usuario>" y password "<password>"
     Then    Clic en boton iniciar sesion y selecciona transferencia a otras cuentas
     When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" monto "<monto>" y descripcion "<descripcion>"
     Then    Se ingresa OTP "<otp>" y se confirma transferencia a otras cuentas
     When    Se confirma y valida resultado "<resultado>" y "<operacion>" a otras cuentas
     Examples: Parametros de entrada
       | usuario | password | origen | destino | moneda | monto | otp | resultado | operacion |
       | 3       | 3        | 3      | 3       | S      | 3     | 3   | 3         | 3         |

   Scenario: Transferencia a otras cuentas Pichincha de una cuenta de dólares a soles
   Verificar que se pueda realizar una transferencia a terceros de una cuenta de dólares a soles

     Given   El cliente abre la pagina de HBK
     When    Se ingresa el usuario "<usuario>" y password "<password>"
     Then    Clic en boton iniciar sesion y selecciona transferencia a otras cuentas
     When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" monto "<monto>" y descripcion "<descripcion>"
     Then    Se ingresa OTP "<otp>" y se confirma transferencia a otras cuentas
     When    Se confirma y valida resultado "<resultado>" y "<operacion>" a otras cuentas
     Examples: Parametros de entrada
       | usuario | password | origen | destino | moneda | monto | otp | resultado | operacion |
       | 4       | 4        | 4      | 4       | D      | 4     | 4   | 4         | 4         |
