package steps;

import bookstores.ReadExcelFile;
import bookstores.WriteExcelFile;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import pageobjects.TransferPage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class TransferSteps {
    private WebDriver driver;
    private WriteExcelFile writeFile;
    private ReadExcelFile readFile;

    @Before
    public void setUp() throws MalformedURLException {
        //Codigo para ejecucion en navegador local//
        //System.setProperty("webdriver.firefox.driver","D:\\PICHINCHA\\ProyectoAutomatizacion\\PryAutomationLogin\\Driver\\geckodriver.exe");
        //driver = new FirefoxDriver();
        //fin de codigo navegador local//

        //Codigo para ejecucion en navegador remoto//
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),new FirefoxOptions());
        //fin de codigo navegador remoto//

        writeFile = new WriteExcelFile();
        readFile = new ReadExcelFile();
    }

    @Given("El cliente abre la pagina de HBK")
    public void el_cliente_abre_la_pagina_de_hbk() {
        driver.get("https://cal-bancadigital.pichincha.pe/login");
    }
    @When("Se ingresa el usuario {string} y password {string}")
    public void se_ingresa_el_usuario_y_password(String usuario, String pass) throws IOException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchUsuario = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(usuario),1));
        String searchPassword = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(pass),2));
        Integer longitud = searchPassword.length();
        transfer.writeUsuario(searchUsuario);
        transfer.clickPassword();
        for (int i = 0; i < longitud; i++)
        {
            transfer.writePassword(String.valueOf(searchPassword.charAt(i)));
        }
    }
    @Then("Clic en boton iniciar sesion e ingresa al Home")
    public void clic_en_boton_iniciar_sesion_e_ingresa_al_home() throws InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        transfer.clickInicio();
        Thread.sleep(7000);
    }
    @When("Clic en transferencia a otros bancos y cuenta origen {string}")
    public void clic_en_transferencia_a_otros_bancos_y_cuenta_origen(String origen) throws InterruptedException, IOException {
        TransferPage transfer = new TransferPage(driver);
        Thread.sleep(3000);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchOrigen = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(origen),3));
        transfer.clickTransferencia();
        Thread.sleep(3000);
        if (Objects.equals(transfer.getObtenerValorNuevaTransferencia(),1)){
            transfer.clickNuevaTransferencia();
            Thread.sleep(4000);
        }
        transfer.clickTransferBanco();
        Thread.sleep(4000);
        transfer.clickSeleccionaOrigen(searchOrigen);
        Thread.sleep(2000);
    }
    @Then("Se ingresa CI {string} monto {string} moneda {string} y descripcion {string}")
    public void se_ingresa_ci_monto_moneda_y_descripcion(String cuentaI, String montoT, String moneda, String descripcionT) throws InterruptedException, IOException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchCuenta = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(cuentaI),4));
        String searchMonto = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(montoT),5));
        String searchDescripcion = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(descripcionT),7));
        String searchTipo = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(descripcionT),9));
        transfer.writeCuentaDestino(searchCuenta);
        Thread.sleep(2000);
        transfer.writeMonto(searchMonto);
        Thread.sleep(2000);
        if (Objects.equals(moneda, "D")){
            transfer.clickSeleccionaMoneda();
        }
        if (Objects.equals(searchTipo,"Transferencia Inmediata")){
            transfer.clickSeleccionaInmediato();
        }
        else
        {
            transfer.clickSeleccionaPorHorarios();
        }
        Thread.sleep(2000);
        transfer.writeDescripcionOperacion(searchDescripcion);
        Thread.sleep(2000);
        transfer.clickSiguientePantalla();
        Thread.sleep(11000);
    }
    @Then("Se ingresa CI {string} invalida")
    public void se_ingresa_ci_invalida(String cuentaI) throws InterruptedException, IOException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchCuenta = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(cuentaI),4));
        String searchErrorCuenta = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(cuentaI),10));
        transfer.writeCuentaDestino(searchCuenta);
        Thread.sleep(3000);
        Assert.assertEquals(transfer.getObtenerErrorCuenta(),searchErrorCuenta);
    }
    @Then("Se ingresa CI {string} y monto no permitido {string}")
    public void se_ingresa_ci_y_monto_no_permitido(String cuentaI, String montoT) throws InterruptedException, IOException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchCuenta = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(cuentaI),4));
        String searchMonto = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(montoT),5));
        String searchErrorCuenta = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(cuentaI),10));
        transfer.writeCuentaDestino(searchCuenta);
        Thread.sleep(3000);
        transfer.writeMonto(searchMonto);
        Thread.sleep(3000);
        Assert.assertEquals(transfer.getObtenerErrorCuenta(),searchErrorCuenta);
    }
    @When("Clic en favorito y cuenta origen {string}")
    public void clic_en_favorito_y_cuenta_origen(String origen) throws InterruptedException, IOException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchOrigen = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(origen),3));
        String searchFavorito = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(origen),8));
        Thread.sleep(3000);
        transfer.clickVerTodoFavorito();
        Thread.sleep(3000);
        transfer.writeBuscaFavorito(searchFavorito);
        Thread.sleep(3000);
        transfer.clickOperacionFavorito();
        Thread.sleep(6000);
        transfer.clickSeleccionaOrigen(searchOrigen);
        Thread.sleep(6000);
    }
    @Then("Se ingresa monto {string} moneda {string} y descripcion {string}")
    public void se_ingresa_monto_moneda_y_descripcion(String montoT, String moneda, String descripcionT) throws IOException, InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchMonto = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(montoT),5));
        String searchDescripcion = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(descripcionT),7));
        String searchTipo = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(descripcionT),9));
        transfer.writeMonto(searchMonto);
        Thread.sleep(2000);
        if (Objects.equals(moneda, "D")){
            transfer.clickSeleccionaMoneda();
        }
        if (Objects.equals(searchTipo,"Transferencia Inmediata")){
            transfer.clickSeleccionaInmediato();
        }
        else
        {
            transfer.clickSeleccionaPorHorarios();
        }
        Thread.sleep(2000);
        transfer.writeDescripcionOperacion(searchDescripcion);
        Thread.sleep(2000);
        transfer.clickSiguientePantalla();
        Thread.sleep(11000);
    }
    @When("Se ingresa OTP {string} y se confirma transferencia")
    public void se_ingresa_otp_y_se_confirma_transferencia(String otp) throws IOException, InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchOTP = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(otp),6));
        if (Objects.equals(transfer.getObtenerValorOTP(),1))
        {
            transfer.writeIngresaOTP(searchOTP);
            Thread.sleep(3000);
        }
        transfer.clickPantallaConfirmar();
        Thread.sleep(8000);
    }
    @Then("Se ingresa OTP {string} y se confirma transferencia a otras cuentas")
    public void se_ingresa_otp_y_se_confirma_transferencia_a_otras_cuentas(String otp) throws InterruptedException, IOException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchOTP = String.valueOf(readFile.getCellValue(filepath, "TransferOtraCuenta", Integer.parseInt(otp),6));
        transfer.writeIngresaOTP(searchOTP);
        Thread.sleep(3000);
        transfer.clickPantallaConfirmar();
        Thread.sleep(6000);
    }
    @When("Se ingresa OTP {string} se guarda favorito {string} y se confirma transferencia")
    public void se_ingresa_otp_se_guarda_favorito_y_se_confirma_transferencia(String otp, String favorito) throws IOException, InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchOTP = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(otp),6));
        String searchFavorito = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(favorito),8));
        if (Objects.equals(transfer.getObtenerValorOTP(),1))
        {
            transfer.writeIngresaOTP(searchOTP);
            Thread.sleep(4000);
        }
        transfer.clickSeleccionaFavorito();
        Thread.sleep(4000);
        transfer.writeFavorito(searchFavorito);
        Thread.sleep(4000);
        transfer.clickGuardarFavorito();
        Thread.sleep(4000);
        transfer.clickPantallaConfirmar();
        Thread.sleep(8000);
    }
    @Then("Se valida el resultado {string} y guarda operacion {string}")
    public void se_valida_el_resultado_y_guarda_operacion(String resultado, String operacion) throws IOException, InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchResultado = String.valueOf(readFile.getCellValue(filepath, "Transfer", Integer.parseInt(resultado),11));

        readFile.readExcel(filepath, "Transfer");
        writeFile.writeCellValue(filepath, "Transfer",Integer.parseInt(operacion),12, transfer.getObtenerOperacion());
        readFile.readExcel(filepath,"Transfer");

        Assert.assertEquals(transfer.getObtenerConfirmacionTransferencia(),searchResultado);
        Thread.sleep(8000);
    }

    //Transferencia propia
    @Then("Clic en boton iniciar sesion y selecciona transferencia propia")
    public void clic_en_boton_iniciar_sesion_y_selecciona_transferencia_propia() throws InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        transfer.clickInicio();
        Thread.sleep(5000);
        transfer.clickTransferencia();
        Thread.sleep(3000);
        if (Objects.equals(transfer.getObtenerValorNuevaTransferencia(),1)){
            transfer.clickNuevaTransferencia();
            Thread.sleep(4000);
        }
        transfer.clickTransferPropia();
        Thread.sleep(4000);
    }
    @When("Selecciona cuenta origen {string} destino {string} moneda {string} y monto {string}")
    public void selecciona_cuenta_origen_destino_moneda_y_monto(String origen, String destino, String moneda, String monto) throws IOException, InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchOrigen = String.valueOf(readFile.getCellValue(filepath, "TransferPropia", Integer.parseInt(origen),3));
        String searchDestino = String.valueOf(readFile.getCellValue(filepath, "TransferPropia", Integer.parseInt(destino),4));
        String searchMonto = String.valueOf(readFile.getCellValue(filepath, "TransferPropia", Integer.parseInt(monto),5));
        transfer.clickSeleccionaOrigen(searchOrigen);
        Thread.sleep(4000);
        transfer.clickSeleccionaDestino(searchDestino);
        Thread.sleep(4000);
        if (Objects.equals(moneda, "D")){
            transfer.clickSeleccionaMoneda();
        }
        transfer.writeMonto(searchMonto);
        Thread.sleep(3000);
        transfer.clickSiguientePantalla();
        Thread.sleep(3000);
        transfer.clickPantallaConfirmar();
        Thread.sleep(4000);
    }

    //Transferencia a otras cuentas
    @Then("Clic en boton iniciar sesion y selecciona transferencia a otras cuentas")
    public void clic_en_boton_iniciar_sesion_y_selecciona_transferencia_a_otras_cuentas() throws InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        transfer.clickInicio();
        Thread.sleep(5000);
        transfer.clickTransferencia();
        Thread.sleep(3000);
        if (Objects.equals(transfer.getObtenerValorNuevaTransferencia(),1)){
            transfer.clickNuevaTransferencia();
            Thread.sleep(4000);
        }
        transfer.clickTransferOtrasCuentas();
        Thread.sleep(7000);
    }
    @When("Selecciona cuenta origen {string} destino {string} moneda {string} monto {string} y descripcion {string}")
    public void selecciona_cuenta_origen_destino_moneda_monto_y_descripcion(String origen, String destino, String moneda, String monto, String descripcion) throws InterruptedException, IOException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchOrigen = String.valueOf(readFile.getCellValue(filepath, "TransferOtraCuenta", Integer.parseInt(origen),3));
        String searchDestino = String.valueOf(readFile.getCellValue(filepath, "TransferOtraCuenta", Integer.parseInt(destino),4));
        String searchMonto = String.valueOf(readFile.getCellValue(filepath, "TransferOtraCuenta", Integer.parseInt(monto),5));
        //String searchOTP = String.valueOf(readFile.getCellValue(filepath, "TransferOtraCuenta", Integer.parseInt(monto),6));
        String searchDescripcion = String.valueOf(readFile.getCellValue(filepath, "TransferOtraCuenta", Integer.parseInt(monto),7));
        transfer.clickSeleccionaOrigen(searchOrigen);
        Thread.sleep(4000);
        transfer.writeOtraCuenta(searchDestino);
        Thread.sleep(4000);
        if (Objects.equals(moneda, "D")){
            transfer.clickSeleccionaMoneda();
        }
        transfer.writeMonto(searchMonto);
        Thread.sleep(3000);
        transfer.writeDescripcionOperacion(searchDescripcion);
        Thread.sleep(3000);
        transfer.clickSiguientePantalla();
        Thread.sleep(5000);
//        transfer.writeIngresaOTP(searchOTP);
//        Thread.sleep(3000);
//        transfer.clickPantallaConfirmar();
//        System.out.println("Se confirmo la transferencia");
//        Thread.sleep(7000);
    }


    @Then("Se confirma y valida resultado {string} y {string}")
    public void se_confirma_y_valida_resultado_y(String mensaje, String operacion) throws IOException, InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchResultado = String.valueOf(readFile.getCellValue(filepath, "TransferPropia", Integer.parseInt(mensaje),6));

        readFile.readExcel(filepath, "TransferPropia");
        writeFile.writeCellValue(filepath, "TransferPropia",Integer.parseInt(operacion),7, transfer.getObtenerOperacion());
        readFile.readExcel(filepath,"TransferPropia");

        Assert.assertEquals(transfer.getObtenerConfirmacionTransferencia(),searchResultado);
        Thread.sleep(8000);
    }
    @When("Se confirma y valida resultado {string} y {string} a otras cuentas")
    public void se_confirma_y_valida_resultado_y_a_otras_cuentas(String mensaje, String operacion) throws IOException, InterruptedException {
        TransferPage transfer = new TransferPage(driver);
        System.out.println("Se ingreso aqui");
        String filepath = "D:\\PICHINCHA\\AUTOMATIZACION\\data\\DataTransferTest.xlsx";
        String searchResultado = String.valueOf(readFile.getCellValue(filepath, "TransferOtraCuenta", Integer.parseInt(mensaje),8));
        System.out.println("Excel: " + searchResultado);
        readFile.readExcel(filepath, "TransferOtraCuenta");
        writeFile.writeCellValue(filepath, "TransferOtraCuenta",Integer.parseInt(operacion),9, transfer.getObtenerOperacion());
        readFile.readExcel(filepath,"TransferOtraCuenta");

        System.out.println("Control: " + transfer.getObtenerConfirmacionTransferencia());
        Assert.assertEquals(transfer.getObtenerConfirmacionTransferencia(),searchResultado);
        Thread.sleep(8000);
    }

    @After
    public void tearDown(Scenario scenario){
        //if (scenario.isFailed())
        takeScreenshot(scenario);
        driver.quit();
    }

    private void takeScreenshot(Scenario scenario){
        final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.attach(screenshot, "image/png","Evidencia");
    }

}
