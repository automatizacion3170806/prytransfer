@TransferenciaPropia
Feature: Proceso de transferencia entre mis cuentas pichincha
  Aqui se realizara las pruebas del proceso de transferencias propias

  Scenario: Transferencia entre cuentas propias de una cuenta de soles a soles
  Verificar que se pueda realizar una transferencia entre cuentas propias de soles a soles

    Given   El cliente abre la pagina de HBK
    When    Se ingresa el usuario "<usuario>" y password "<password>"
    Then    Clic en boton iniciar sesion y selecciona transferencia propia
    When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" y monto "<monto>"
    Then    Se confirma y valida resultado "<resultado>" y "<operacion>"
    Examples: Parametros de entrada
      | usuario | password | origen | destino | moneda | monto | resultado | operacion |
      | 1       | 1        | 1      | 1       | S      | 1     | 1         | 1         |

  Scenario: Transferencia entre cuentas propias de una cuenta de dolares a dolares
  Verificar que se pueda realizar una transferencia entre cuentas propias de dolares a dolares

    Given   El cliente abre la pagina de HBK
    When    Se ingresa el usuario "<usuario>" y password "<password>"
    Then    Clic en boton iniciar sesion y selecciona transferencia propia
    When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" y monto "<monto>"
    Then    Se confirma y valida resultado "<resultado>" y "<operacion>"
    Examples: Parametros de entrada
      | usuario | password | origen | destino | moneda | monto | resultado | operacion |
      | 2       | 2        | 2      | 2       | S      | 2     | 2         | 2         |
#
#  Scenario: Transferencia entre cuentas propias de una cuenta de soles a dolares
#  Verificar que se pueda realizar una transferencia entre cuentas propias de soles a dolares
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion y selecciona transferencia propia
#    When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" y monto "<monto>"
#    Then    Se confirma y valida resultado "<resultado>" y "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | destino | moneda | monto | resultado | operacion |
#      | 3       | 3        | 3      | 3       | D      | 3     | 3         | 3         |
#
#  Scenario: Transferencia entre cuentas propias de una cuenta de dolares a soles
#  Verificar que se pueda realizar una transferencia entre cuentas propias de dolares a soles
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion y selecciona transferencia propia
#    When    Selecciona cuenta origen "<origen>" destino "<destino>" moneda "<moneda>" y monto "<monto>"
#    Then    Se confirma y valida resultado "<resultado>" y "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | destino | moneda | monto | resultado | operacion |
#      | 4       | 4        | 4      | 4       | S      | 4     | 4         | 4         |
