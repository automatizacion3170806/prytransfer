@TransferenciaInterbancaria
Feature: Proceso de transferencia a otros bancos desde una cuenta pichincha
  Aqui se realizara las pruebas del proceso de transferencia a otros bancos

  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a soles
    Verificar que se pueda realizar una transferencia a otros bancos inmediata de una cuenta de soles a soles

    Given   El cliente abre la pagina de HBK
    When    Se ingresa el usuario "<usuario>" y password "<password>"
    Then    Clic en boton iniciar sesion e ingresa al Home
    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
    When    Se ingresa OTP "<otp>" y se confirma transferencia
    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
    Examples: Parametros de entrada
      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
      | 1       | 1        | 1      | 1      | 1     | S      | 1           | 1         | 1   | 1         |

  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a soles y guardarla como favoritos
  Verificar que se pueda realizar una transferencia a otros bancos inmediata de una cuenta de soles a soles y grabarla como favoritos

    Given   El cliente abre la pagina de HBK
    When    Se ingresa el usuario "<usuario>" y password "<password>"
    Then    Clic en boton iniciar sesion e ingresa al Home
    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
    When    Se ingresa OTP "<otp>" se guarda favorito "<favorito>" y se confirma transferencia
    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
    Examples: Parametros de entrada
      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | favorito | resultado |
      | 2       | 2        | 2      | 2      | 2     | S      | 2           | 2         | 2   | 2        | 2         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de dolares a dolares
#  Verificar que se pueda realizar una transferencia a otros bancos inmediata de una cuenta de dólares a dólares
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
#      | 3       | 3        | 3      | 3      | 3     | D      | 3           | 3         | 3   | 3         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de dolares a soles
#  Verificar que se pueda realizar una transferencia a otros bancos inmediata de una cuenta de dólares a soles
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
#      | 4       | 4        | 4      | 4      | 4     | S      | 4           | 4         | 4   | 4         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a dolares
#  Verificar que se pueda realizar una transferencia a otros bancos inmediata de una cuenta de soles a dólares
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
#      | 5       | 5        | 5      | 5      | 5     | D      | 5           | 5         | 5   | 5         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a soles donde la cuenta destino es invalida
#    Verificar que no se pueda realizar una transferencia a interbancaria inmediata de soles a soles donde la cuenta destino sea inválida
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" invalida
#    Examples: Parametros de entrada
#     | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
#     | 6       | 6        | 6      | 6      | 6     | S      | 6           | 6         | 6   | 6         |
#
#  Scenario: Transferencia a otros bancos diferida de una cuenta de soles a soles
#    Verificar que se pueda realizar una transferencia a otros bancos diferida de una cuenta de soles a soles
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
#      | 7       | 7        | 7      | 7      | 7     | S      | 7           | 7         | 7   | 7         |
#
#  Scenario: Transferencia a otros bancos diferida de una cuenta de dolares a dolares
#  Verificar que se pueda realizar una transferencia a otros bancos diferida de una cuenta de dólares a dólares
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
#      | 8       | 8        | 8      | 8      | 8     | D      | 8           | 8         | 8   | 8         |
#
#  Scenario: Transferencia a otros bancos diferida de una cuenta de soles a dolares
#  Verificar que se pueda realizar una transferencia a otros bancos diferida de una cuenta de soles a dólares
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario | password | origen | cuenta | monto | moneda | descripcion | operacion | otp | resultado |
#      | 9       | 9        | 9      | 9      | 9     | D      | 9           | 9         | 9   | 9         |
#
#  Scenario: Transferencia a otros bancos diferida de una cuenta de dolares a soles
#  Verificar que se pueda realizar una transferencia a otros bancos diferida de una cuenta de dólares a soles
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario  | password  | origen  | cuenta  | monto  | moneda | descripcion  | operacion  | otp  | resultado  |
#      | 10       | 10        | 10      | 10      | 10     | S      | 10           | 10         | 10   | 10         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a otra cuenta de soles que no existe
#    Verificar que se pueda realizar una transferencia a otros bancos inmediata de una cuenta de soles a otra cuenta de soles que no existe.
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    Examples: Parametros de entrada
#      | usuario  | password  | origen  | cuenta  | monto  | moneda | descripcion  | operacion  | otp  | resultado  |
#      | 11       | 11        | 11      | 11      | 11     | S      | 11           | 11         | 11   | 11         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a soles donde el monto supera el limite permitido
#    Verificar que no se pueda realizar una transferencia a interbancaria inmediata de soles a soles donde el monto supera el limite permitido
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" y monto no permitido "<monto>"
#    Examples: Parametros de entrada
#      | usuario  | password  | origen  | cuenta  | monto  | moneda | descripcion  | operacion  | otp  | resultado  |
#      | 12       | 12        | 12      | 12      | 12     | S      | 12           | 12         | 12   | 12         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a otra cuenta de soles que esta bloqueada
#    Verificar que se pueda realizar una transferencia a otros bancos inmediata de una cuenta de soles a otra cuenta de soles que esta bloqueada
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en transferencia a otros bancos y cuenta origen "<origen>"
#    Then    Se ingresa CI "<cuenta>" monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    Examples: Parametros de entrada
#      | usuario  | password  | origen  | cuenta  | monto  | moneda | descripcion  | operacion  | otp  | resultado  |
#      | 13       | 13        | 13      | 13      | 13     | S      | 13           | 13         | 13   | 13         |
#
#  Scenario: Transferencia a otros bancos inmediata de una cuenta de soles a soles desde un favorito
#  Verificar que se pueda ingresar desde un favorito para realizar una transferencia a otros bancos inmediata de una cuenta de soles a soles
#
#    Given   El cliente abre la pagina de HBK
#    When    Se ingresa el usuario "<usuario>" y password "<password>"
#    Then    Clic en boton iniciar sesion e ingresa al Home
#    When    Clic en favorito y cuenta origen "<origen>"
#    Then    Se ingresa monto "<monto>" moneda "<moneda>" y descripcion "<descripcion>"
#    When    Se ingresa OTP "<otp>" y se confirma transferencia
#    Then    Se valida el resultado "<resultado>" y guarda operacion "<operacion>"
#    Examples: Parametros de entrada
#      | usuario  | password  | origen  | cuenta  | monto  | moneda | descripcion  | operacion  | otp  | resultado  |
#      | 14       | 14        | 14      | 14      | 14     | S      | 14           | 14         | 14   | 14         |